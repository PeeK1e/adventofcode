# Advent of Code

This repo collects my (PeeK1e) solutions for [advent of code](https://adventofcode.com).

To access them select the `year`/`day`/`task no.` beginning from the repository root.