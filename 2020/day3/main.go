package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"strconv"
	"time"
)

func main()  {
	start := time.Now()
	treeMap := readMap("./input")

	x, y, xl, yl, c:= 0,0, len(treeMap[0]) , len(treeMap)-1, 0
	for y < yl {
		x += 3
		x %= xl
		y += 1
		if treeMap[y][x] == '#' {
			c++
		}
	}
	fmt.Println(time.Now().Sub(start))
	fmt.Println("Trees: " + strconv.Itoa(c))
}

func readMap(path string) []string{
	file, err := os.Open(path)
	if err != nil {
		log.Fatalln("Could not open file")
	}
	defer file.Close()

	var lines []string
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		lines = append(lines, scanner.Text())
	}
	return lines
}
