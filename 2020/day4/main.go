package main

import (
	"bufio"
	"fmt"
	"os"
	"regexp"
	"strconv"
	"strings"
	"time"
)

func main() {
	start := time.Now()
	_inputSplitMap := getInputMap("./input")

	count := 0

	for _, inputSplitMap := range _inputSplitMap {
		if inputSplitMap["byr"] == "" {
			continue
		} else {
			yr, err := strconv.Atoi(inputSplitMap["byr"])
			if err != nil {
				continue
			}
			if !(yr >= 1920 && yr <= 2002) {
				continue
			}
		}
		if inputSplitMap["iyr"] == "" {
			continue
		} else {
			yr, err := strconv.Atoi(inputSplitMap["iyr"])
			if err != nil {
				continue
			}
			if !(yr >= 2010 && yr <= 2020) {
				continue
			}
		}
		if inputSplitMap["eyr"] == "" {
			continue
		} else {
			yr, err := strconv.Atoi(inputSplitMap["eyr"])
			if err != nil {
				continue
			}
			if !(yr >= 2020 && yr <= 2030) {
				continue
			}
		}
		if inputSplitMap["hgt"] == "" {
			continue
		} else {
			str := inputSplitMap["hgt"]
			if strings.Contains(str, "cm") {
				str = strings.ReplaceAll(str, "cm", "")
				h, err := strconv.Atoi(str)
				if err != nil {
					continue
				}
				if !(h >= 150 && h <= 193) {
					continue
				}
			} else if strings.Contains(str, "in") {
				str = strings.ReplaceAll(str, "in", "")
				h, err := strconv.Atoi(str)
				if err != nil {
					continue
				}
				if !(h >= 59 && h <= 76) {
					continue
				}
			} else {
				continue
			}
		}
		if inputSplitMap["hcl"] == "" {
			continue
		} else {
			str := inputSplitMap["hcl"]
			if strings.Contains(str, "#") {
				r := regexp.MustCompilePOSIX(`^#([a-fA-F0-9]{6})$`)
				if !r.Match([]byte(str)) {
					continue
				}
			} else {
				continue
			}
		}
		if inputSplitMap["ecl"] == "" {
			continue
		} else {
			str := inputSplitMap["ecl"]
			if !(str == "amb" || str == "blu" || str == "brn" || str == "gry" || str == "grn" || str == "hzl" || str == "oth") {
				continue
			}
		}
		if inputSplitMap["pid"] == "" {
			continue
		} else {
			str := inputSplitMap["pid"]
			if !(len(str) == 9) {
				continue
			}
		}
		count++
		//fmt.Printf("Correct Passport: %s \n", inputSplitMap)
	}

	fmt.Printf("Valid Passports: %d, Total Passports: %d, Took %s", count, len(_inputSplitMap), time.Now().Sub(start))
}

func getInputMap(path string) []map[string]string {
	file, err := os.Open(path)
	if err != nil {
		fmt.Println("Could not open File")
	}

	defer file.Close()

	scanner := bufio.NewScanner(file)
	passportSplitMap := make([]map[string]string,400)
	var passportString string

	c, ok := 0, true
	for ok {
		ok = scanner.Scan()
		line := scanner.Text()
		if line == "" {
			splitString := strings.SplitAfter(passportString, " ")

			tempStringMap := make(map[string]string)
			for i := range splitString {
				s := strings.ReplaceAll(splitString[i], " ", "")
				if s == "" {
					continue
				}
				split := strings.Split(s, ":")
				tempStringMap[split[0]] = split[1]
			}

			passportSplitMap = append(passportSplitMap, tempStringMap)

			passportString = ""
			c++
		} else {
			passportString = passportString + " " + line
		}
	}

	return passportSplitMap
}
