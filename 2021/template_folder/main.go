package main

import (
	"bufio"
	"log"
	"os"
)

func main()  {
	file, err := os.Open("./input/input")
	if err != nil {
		log.Panicf("Error Opening File %s ", err)
	}
	scanner := bufio.NewScanner(file)

	for ok := scanner.Scan(); ok; ok = scanner.Scan() {
        line := scanner.Text()
		_ = line
	}

}
