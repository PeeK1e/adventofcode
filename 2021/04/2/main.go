package main

import (
	"bufio"
	"log"
	"os"
	"strconv"
	"strings"
)

type field struct {
	value  int
	marked bool
}

func main() {
	file, err := os.Open("./input/input")
	if err != nil {
		log.Panicf("Error Opening File %s ", err)
	}
	scanner := bufio.NewScanner(file)

	scanner.Scan()
	numberChars := strings.Split(scanner.Text(), ",")
	numbers := make([]int, 0)
	for _, numberChar := range numberChars {
		v, err := strconv.Atoi(numberChar)
		if err != nil {
			continue
		}
		numbers = append(numbers, v)
	}

	boards := make([][][]field, 0) //List of all Bingo Boards

	scanner.Scan() // To get rid of first whitespace after number line

	for tempMap, ok := make([][]field, 0), scanner.Scan(); ok; ok = scanner.Scan() {
		line := scanner.Text()

		if line == "" {
			boards = append(boards, tempMap)
			tempMap = make([][]field, 0)
			continue
		}

		split := strings.Split(line, " ")

		row := make([]field, 0)
		for _, s := range split {
			v, err := strconv.Atoi(s)
			if err != nil {
				continue
			}

			row = append(row, field{
				value:  v,
				marked: false,
			})
		}
		tempMap = append(tempMap, row)
	}

	pullNumbers(numbers, boards)

}

func checkRow(board [][]field) bool {
	for _, row := range board {
		found := true
		for _, i3 := range row {
			if !i3.marked {
				found = false
				break
			}
		}
		if found {
			return true
		}
	}
	return false
}

func checkCol(board [][]field) bool {
	for i := 0; i < len(board[0]); i++ {
		found := true
		for j := 0; j < len(board); j++ {
			if !board[j][i].marked {
				found = false
				break
			}
		}
		if found {
			return true
		}
	}
	return false
}

func pullNumbers(numberList []int, boards [][][]field) {
	for _, n := range numberList {
		for i := 0; i < len(boards); i++ {
			updateBoard(&(boards[i]), n)
		}
		getWinningBoard(&boards, n)
	}
}

//Does this crap work? update: yes it does.
func updateBoard(board *[][]field, number int) {
	for i := 0; i < len(*board); i++ {
		for j := 0; j < len((*board)[i]); j++ {
			if (*board)[i][j].value == number {
				(*board)[i][j].marked = true
			}
		}
	}
}

func getWinningBoard(boards *[][][]field, lastCalled int) {
	for i := 0; i < len(*boards); i++ {
		if checkCol((*boards)[i]) || checkRow((*boards)[i]) {
			log.Printf("Found board nr. %d, Score %d, Winning Number %d", i, getScore((*boards)[i], lastCalled), lastCalled)
			*boards = append((*boards)[:i], (*boards)[i+1:]...)
		}
	}
}

func getScore(board [][]field, lastCalled int) int {
	sum := 0
	for _, fields := range board {
		for _, f := range fields {
			if !f.marked {
				sum += f.value
			}
		}
	}
	return sum * lastCalled
}
