package main

import (
	"bufio"
	"log"
	"os"
	"strconv"
	"strings"
)

type field struct {
	value  int
	marked bool
}

func main() {
	file, err := os.Open("./input/input")
	if err != nil {
		log.Panicf("Error Opening File %s ", err)
	}
	scanner := bufio.NewScanner(file)

	scanner.Scan()
	numberChars := strings.Split(scanner.Text(), ",")
	numbers := make([]int, len(numberChars))
	for i, numberChar := range numberChars {
		v, _ := strconv.Atoi(numberChar)
		numbers[i] = v
	}

	//List of Bingo Boards
	boards := make([][][]field, 0)

	scanner.Scan()
	for tempMap, ok := make([][]field, 0), scanner.Scan(); ok; ok = scanner.Scan() {
		line := scanner.Text()

		if line == "" {
			boards = append(boards, tempMap)
			tempMap = make([][]field, 0)
			continue
		}

		split := strings.Split(line, " ")

		row := make([]field, 0)
		for _, s := range split {
			v, err := strconv.Atoi(s)
			if err != nil {
				continue
			}

			row = append(row, field{
				value:  v,
				marked: false,
			})
		}
		tempMap = append(tempMap, row)
	}

	pullNumbers(numbers, boards)

}

func checkRow(board [][]field) bool {
	for _, ints := range board {
		found := true
		for _, i3 := range ints {
			if !i3.marked {
				found = false
				break
			}
		}
		if found {
			return true
		}
	}
	return false
}

func checkCol(board [][]field) bool {
	for i := 0; i < len(board[0]); i++ {
		found := true
		for j := 0; j < len(board); j++ {
			if !board[j][i].marked {
				found = false
				break
			}
		}
		if found {
			return true
		}
	}
	return false
}

func pullNumbers(numberList []int, boards [][][]field) {
	for _, n := range numberList {
		for i := 0; i < len(boards); i++ {
			updateBoard(&boards[i], n)
		}
		getWinningBoard(boards, n)
	}
}

//Does this crap work? update: yes it does.
func updateBoard(board *[][]field, number int) {
	for i := 0; i < len(*board); i++ {
		for j := 0; j < len((*board)[i]); j++ {
			if (*board)[i][j].value == number {
				(*board)[i][j].marked = true
			}
		}
	}
}

func getWinningBoard(boards [][][]field, lastCalled int) {
	for i, board := range boards {
		if checkRow(board) || checkCol(board) {
			log.Printf("Found board nr. %d, Score %d, Winning Number %d", i, getScore(board, lastCalled), lastCalled)
			os.Exit(0)
		}
	}
}

func getScore(board [][]field, lastCalled int) int {
	sum := 0
	for _, fields := range board {
		for _, f := range fields {
			if !f.marked {
				sum += f.value
			}
		}
	}
	return sum * lastCalled
}
