package main

import (
	"bufio"
	"log"
	"os"
	"strconv"
    "time"
)

func main() {
	start, valueMap := time.Now(), make([]int, 0)
	scanner := bufio.NewScanner(func(f *os.File, e error) *os.File { return f }(os.Open("./input/input")))
	for ok := scanner.Scan(); ok; func(i int, e error) { valueMap = append(valueMap, i); ok = scanner.Scan() }(strconv.Atoi(scanner.Text())) {}
	valueSumMap := make([]int, len(valueMap))
	valueMap[0] = valueMap[0]
	for i := 1; i < len(valueMap)-2; i++ {valueSumMap[i] += func(a []int) int {s := 0;for _, v := range a {s += v}; return s;}(valueMap[i-1 : i+2]);}
	prev, count := valueSumMap[0], 0
	for _, i2 := range valueSumMap {if (i2 > prev) {count++;}; prev = i2;}
	end := time.Since(start)
	log.Printf("Increases %d times, Time: %s", count, end)
}
