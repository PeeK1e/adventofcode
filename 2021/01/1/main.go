package main

import (
	"bufio"
	"log"
	"os"
	"strconv"
)

func main()  {
	file, err := os.Open("./input/input")
	if err != nil {
		log.Panicf("Error Opening File %s ", err)
	}
	scanner := bufio.NewScanner(file)

	c := 0
	for ok, oldi := scanner.Scan(), -1; ok; ok = scanner.Scan() {
		newi, _ := strconv.Atoi(scanner.Text())

		if oldi == -1 {
			oldi = newi
			continue
		}

		if newi > oldi {
			c++
		}

		oldi = newi
	}

	log.Printf("Increases %d times", c)
}
