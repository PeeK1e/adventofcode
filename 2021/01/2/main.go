package main

import (
	"bufio"
	"log"
	"os"
	"strconv"
    "time"
)

func main()  {
    start := time.Now()
	file, err := os.Open("./input/input")
	if err != nil {
		log.Panicf("Error Opening File %s ", err)
	}
	scanner := bufio.NewScanner(file)

	valueMap, valueSumMap  := make([]int, 0), make([]int, 0)
	for ok := scanner.Scan() ; ok; ok = scanner.Scan() {
		value, _ := strconv.Atoi(scanner.Text())
		valueMap = append(valueMap, value)
	}

	for i, _ := range valueMap {
		value := 0
		for j := 0; j < 3; j++ {
			if (i+j) < len(valueMap){
				value += valueMap[i+j]
			}
		}
		valueSumMap = append(valueSumMap, value)
	}

	prev, count := valueSumMap[0], 0
	for _, i2 := range valueSumMap {
		if i2 > prev {
			count++
		}
		prev = i2
	}
    end := time.Since(start)
	log.Printf("Increases %d times, Time: %s", count, end)
}
