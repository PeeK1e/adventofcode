package main

import (
	"bufio"
	"log"
	"os"
	"strconv"
	"strings"
)

func main()  {
	file, err := os.Open("./input/input")
	if err != nil {
		log.Panicf("Error Opening File %s ", err)
	}
	scanner := bufio.NewScanner(file)

	xPos, depth ,aim := 0, 0, 0
	for ok := scanner.Scan(); ok; ok = scanner.Scan() {
		line := scanner.Text()
		split := strings.Split(line, " ")
		switch split[0] {
			case "forward":
				n, _ := strconv.Atoi(split[1])
				xPos += n
				depth += aim * n
				break
			case "up":
				n, _ := strconv.Atoi(split[1])
				aim -= n
				break
			case "down":
				n, _ := strconv.Atoi(split[1])
				aim += n
				break
		}
	}
	log.Printf("x: %d aim: %d, depth: %d, Position: %d", xPos, aim, depth, xPos * depth)
}
