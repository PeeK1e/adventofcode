package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"strconv"
	"strings"
)

func main() {
	file, err := os.Open("./input/input")
	if err != nil {
		log.Panicf("Error Opening File %s ", err)
	}
	scanner := bufio.NewScanner(file)

	pointMap := make([][][]int, 0)
	oceanMap := make([][]int, 0)

	for ok := scanner.Scan(); ok; ok = scanner.Scan() {
		tempMap := make([][]int, 2)
		tempPointsBegin := make([]int, 2)
		tempPointsEnd := make([]int, 2)

		line := scanner.Text()
		points := strings.Split(line, " -> ")

		begin := strings.Split(points[0], ",")
		end := strings.Split(points[1], ",")

		tempPointsBegin[1], _ = strconv.Atoi(begin[0])
		tempPointsBegin[0], _ = strconv.Atoi(begin[1])
		tempPointsEnd[1], _ = strconv.Atoi(end[0])
		tempPointsEnd[0], _ = strconv.Atoi(end[1])

		tempMap[0] = tempPointsBegin
		tempMap[1] = tempPointsEnd

		pointMap = append(pointMap, tempMap)
	}

	initMap(&oceanMap, 1000)

	drawMap(pointMap, &oceanMap)

	//printMap(oceanMap)

	fmt.Printf("Found %d overlaps", countOverlaps(oceanMap))

}

func initMap(oceanMap *[][]int, size int) {
	*oceanMap = make([][]int, size, size)
	for i := 0; i < len(*oceanMap); i++ {
		(*oceanMap)[i] = make([]int, size, size)
	}
}

func drawMap(pointMap [][][]int, oceanMap *[][]int) {
	for _, i2 := range pointMap {
		x1, y1, x2, y2 := i2[0][0], i2[0][1], i2[1][0], i2[1][1]

		if ((x1 - x2) != 0) && ((y1 - y2) != 0) {
			continue
		}
		if x2-x1 > 0 {
			for i := 0; i <= (x2 - x1); i++ {
				(*oceanMap)[x1+i][y1]++
			}
		} else if x2-x1 < 0 {
			for i := x1 - x2; i >= 0; i-- {
				(*oceanMap)[x2+i][y1]++
			}
		}

		if y2-y1 > 0 {
			for i := 0; i <= (y2 - y1); i++ {
				(*oceanMap)[x1][y1+i]++
			}
		} else if y2-y1 < 0 {
			for i := y1 - y2; i >= 0; i-- {
				(*oceanMap)[x1][y2+i]++
			}
		}
	}
}

func countOverlaps(oceanMap [][]int) int {
	c := 0
	for _, ints := range oceanMap {
		for _, v := range ints {
			if v > 1 {
				c++
			}
		}
	}
	return c
}

func printMap(oceanMap [][]int) {
	for _, ints := range oceanMap {
		for _, i2 := range ints {
			if i2 == 0 {
				fmt.Print(".")
			} else {
				fmt.Print(i2)
			}
		}
		fmt.Println()
	}
}
