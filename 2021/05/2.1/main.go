package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"strconv"
	"strings"
	"time"
)

type point struct {
	direction string
	amount    int
	X         int
	Y         int
}

func main() {
	start := time.Now()
	file, err := os.Open("./input/input")
	if err != nil {
		log.Panicf("Error Opening File %s ", err)
	}
	scanner := bufio.NewScanner(file)

	pointMap := make([]point, 0)
	oceanMap := make([][]int, 0)

	for ok := scanner.Scan(); ok; ok = scanner.Scan() {
		tPoint := point{
			direction: "",
			amount:    0,
		}

		line := scanner.Text()
		points := strings.Split(line, " -> ")

		begin := strings.Split(points[0], ",")
		end := strings.Split(points[1], ",")

		beginX, _ := strconv.Atoi(begin[0])
		beginY, _ := strconv.Atoi(begin[1])
		endX, _ := strconv.Atoi(end[0])
		endY, _ := strconv.Atoi(end[1])

		if (endY - beginY) > 0 {
			tPoint.direction += "d"
		} else if (endY - beginY) < 0 {
			tPoint.direction += "u"
		}

		if (endX - beginX) > 0 {
			tPoint.direction += "r"
		} else if (endX - beginX) < 0 {
			tPoint.direction += "l"
		}

		amount := endX - beginX
		if amount == 0 {
			amount = endY - beginY
		}
		if amount < 0 {
			amount *= -1
		}
		tPoint.amount = amount
		tPoint.X = beginX
		tPoint.Y = beginY

		pointMap = append(pointMap, tPoint)
	}

	initMap(&oceanMap, 1000)

	drawMap(pointMap, &oceanMap)

	//printStructs(pointMap)
	//printMap(oceanMap)

	duration := time.Since(start)

	fmt.Printf("Found %d overlaps, took %s", countOverlaps(oceanMap), duration)

}

func initMap(oceanMap *[][]int, size int) {
	*oceanMap = make([][]int, size, size)
	for i := 0; i < len(*oceanMap); i++ {
		(*oceanMap)[i] = make([]int, size, size)
	}
}

func drawMap(pointMap []point, oceanMap *[][]int) {
	for _, i2 := range pointMap {
		switch i2.direction {
		case "u":
			for i := 0; i <= i2.amount; i++ {
				(*oceanMap)[i2.X][i2.Y-i]++
			}
			break
		case "d":
			for i := 0; i <= i2.amount; i++ {
				(*oceanMap)[i2.X][i2.Y+i]++
			}
			break
		case "l":
			for i := 0; i <= i2.amount; i++ {
				(*oceanMap)[i2.X-i][i2.Y]++
			}
			break
		case "r":
			for i := 0; i <= i2.amount; i++ {
				(*oceanMap)[i2.X+i][i2.Y]++
			}
			break
		case "ul":
			for i := 0; i <= i2.amount; i++ {
				(*oceanMap)[i2.X-i][i2.Y-i]++
			}
			break
		case "ur":
			for i := 0; i <= i2.amount; i++ {
				(*oceanMap)[i2.X+i][i2.Y-i]++
			}
			break
		case "dl":
			for i := 0; i <= i2.amount; i++ {
				(*oceanMap)[i2.X-i][i2.Y+i]++
			}
			break
		case "dr":
			for i := 0; i <= i2.amount; i++ {
				(*oceanMap)[i2.X+i][i2.Y+i]++
			}
			break
		}
	}
}

func countOverlaps(oceanMap [][]int) int {
	c := 0
	for _, ints := range oceanMap {
		for _, v := range ints {
			if v > 1 {
				c++
			}
		}
	}
	return c
}

func printMap(oceanMap [][]int) {
	fmt.Print("  ")
	for i := 0; i < len(oceanMap[0]); i++ {
		fmt.Print(i)
	}
	for i, ints := range oceanMap {
		fmt.Print("\n" + strconv.Itoa(i) + " ")
		for _, i2 := range ints {
			if i2 == 0 {
				fmt.Print(".")
			} else {
				fmt.Print(i2)
			}
		}
	}
	fmt.Println("")
}

func printStructs(pieces []point) {
	for i, piece := range pieces {
		fmt.Printf("No.: %d,\t Dir: %s,\t Amount: %d,\t X: %d,\t Y: %d \n", i, piece.direction, piece.amount, piece.X, piece.Y)
	}
}
