package main

import (
	"bufio"
	"log"
	"os"
	"strconv"
)

func main() {
	file, err := os.Open("./input/input")
	if err != nil {
		log.Panicf("Error Opening File %s ", err)
	}
	scanner := bufio.NewScanner(file)

	list := make([]string, 0)
	for ok := scanner.Scan(); ok; ok = scanner.Scan() {
		list = append(list, scanner.Text())
	}

	mostBits, leastBits := list, list

	for i := 0; i < len(mostBits[0]); i++ {
		tmpList := make([]string, 0)
		bit, _ := getBitsForPos(mostBits, i)
		for _, v := range mostBits {
			if v[i] == bit {
				tmpList = append(tmpList, v)
			}
		}
		mostBits = tmpList
		if len(mostBits) == 1 {
			break
		}
	}

	for i := 0; i < len(leastBits[0]); i++ {
		tmpList := make([]string, 0)
		_, bit := getBitsForPos(leastBits, i)
		for _, v := range leastBits {
			if v[i] == bit {
				tmpList = append(tmpList, v)
			}
		}
		leastBits = tmpList
		if len(leastBits) == 1 {
			break
		}
	}

	g, _ := strconv.ParseInt(mostBits[0], 2, 64)
	e, _ := strconv.ParseInt(leastBits[0], 2, 64)

	log.Printf("Epsilon: %d, Gamma: %d, Consumption %d", e, g, e*g)
}

func getBitsForPos(list []string, pos int) (uint8, uint8) {
	c := 0
	for _, s := range list {
		if s[pos] == '1' {
			c++
		} else {
			c--
		}
	}
	if c >= 0 {
		return '1', '0'
	} else {
		return '0', '1'
	}
}
