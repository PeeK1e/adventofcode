package main

import (
	"bufio"
	"log"
	"os"
	"strconv"
)

func main()  {
	file, err := os.Open("./input/input")
	if err != nil {
		log.Panicf("Error Opening File %s ", err)
	}
	scanner := bufio.NewScanner(file)

	arr := make([]int, 12, 12)

	for ok := scanner.Scan(); ok; ok = scanner.Scan() {
      line := scanner.Text()
	  for i := 0; i < len(line); i++ {
		  if line[i] == '1' {
			  arr[i]++
		  } else {
			  arr[i]--
		  }
	  }
	}

	gamma, epsilon := "", ""
	for _, i2 := range arr {
		if i2 > 0 {
			gamma += "1"
			epsilon += "0"
		} else {
			gamma += "0"
			epsilon += "1"
		}
	}

	g, _ := strconv.ParseInt(gamma,2,64)
	e, _ := strconv.ParseInt(epsilon,2,64)

	log.Printf("Epsilon: %d, Gamma: %d, Consumption %d", e, g, e*g)

}
