package main

import (
	"bufio"
	"fmt"
	"log"
	"math"
	"os"
	"strconv"
	"strings"
	"time"
)

func main() {
	file, err := os.Open("./input/input")
	if err != nil {
		log.Panicf("Error Opening File %s ", err)
	}

	start := time.Now()

	scanner := bufio.NewScanner(file)

	crabPos := make([]int, 0)
	sum := 0
	median := 0

	for ok := scanner.Scan(); ok; ok = scanner.Scan() {
        line := scanner.Text()
		split := strings.Split(line, ",")
		for _, s := range split {
			n, _ := strconv.Atoi(s)
			crabPos = append(crabPos, n)
			sum += n
		}
	}

	median = sum / len(crabPos)

	//something something gradual descent algorithm
	min := getFuelConsumptionToPoint(crabPos, median)
	for count := 1; true; count++ {
		consumption := getFuelConsumptionToPoint(crabPos, median + count)
		if consumption < min {
			min = consumption
		} else {
			consumption = getFuelConsumptionToPoint(crabPos, median + (count * -1))
			if consumption < min {
				min = consumption
			} else {
				break
			}
		}
	}
	duration := time.Since(start)
	fmt.Printf("Least Consumption %d, Median %d, took: %s", min, median, duration)
}

func getFuelConsumptionToPoint(positions []int, point int) int {
	consumption := 0
	for _, position := range positions {
		singleCrab := int(math.Abs(float64(position - point)))
		for i, limit := 1, singleCrab; i < limit; i++ {
			singleCrab += i
		}
		consumption += singleCrab
	}
	return consumption
}