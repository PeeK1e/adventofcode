package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"strconv"
	"strings"
)

func main() {
	file, err := os.Open("./input/input")
	if err != nil {
		log.Panicf("Error Opening File %s ", err)
	}
	scanner := bufio.NewScanner(file)

	fishTimer := make([]int, 0)

	for ok := scanner.Scan(); ok; ok = scanner.Scan() {
		line := scanner.Text()
		split := strings.Split(line, ",")
		for _, s := range split {
			n, _ := strconv.Atoi(s)
			fishTimer = append(fishTimer, n)
		}
	}

	for i := 0; i < 80; i++ {
		listLen := len(fishTimer)
		for i := 0; i < listLen; i++ {
			fishTimer[i]--
			if fishTimer[i] < 0 {
				fishTimer[i] = 6
				fishTimer = append(fishTimer, 8)
			}
		}
	}

	fmt.Printf("Ammount of fish: %d", len(fishTimer))
}
