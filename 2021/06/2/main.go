package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"strconv"
	"strings"
	"time"
)

func main() {
	start := time.Now()
	file, err := os.Open("./input/input")
	if err != nil {
		log.Panicf("Error Opening File %s ", err)
	}
	scanner := bufio.NewScanner(file)

	count := make([]int64, 9)

	for ok := scanner.Scan(); ok; ok = scanner.Scan() {
		line := scanner.Text()
		split := strings.Split(line, ",")
		for _, s := range split {
			n, _ := strconv.Atoi(s)
			count[n]++
		}
	}

	for i := 0; i < 256; i++ {
		newFishes := count[0]

		for j := 0; j < len(count)-1; j++ {
			count[j] = count[j+1]
		}
		count[8] = newFishes
		count[6] += newFishes
	}

	sum := int64(0)
	for _, i2 := range count {
		sum += i2
	}
	time := time.Since(start)

	fmt.Printf("Ammount of fish: %d, time: %s", sum, time)
}
